<?php
	require("class-product-price.php");
	$prices = [];
	
	if (class_exists('MyClass')) 
	{
		echo( 'Could not include the product price class' );
		exit();
	}
	else 
	{
		$prices[] = new product_Price( 200, 'EUR', 'SKU-11334' );
		$prices[] = new product_Price( 250, 'USD', 'SKU-11334' );
		$prices[] = new product_Price( 252, 'USD', 'SKU-11335' );
		$prices[] = new product_Price( 550, 'USD', 'SKU-11336' );
		$prices[] = new product_Price( 530, 'USD', 'SKU-11337' );
		$prices[] = new product_Price( 133, 'USD', 'SKU-11338' );
		$prices[] = new product_Price( 441, 'USD', 'SKU-11339' );
		$prices[] = new product_Price( 867, 'USD', 'SKU-11341' );
	}
	
	//gives ability to overwrite price
	$prices[3]->setPrice( 250, 15 );

	$quantity = 55;
?>
<!DOCTYPE html>
<html>
<head>
	<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
	<script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/markdown-it/8.0.0/markdown-it.min.js"></script>
	<meta charset="utf-8">
	<title>Developer Test</title>
</head>
<body>
	<div class="container">
		<div class="row">
			<div class="col">
				<section id="information"></section>
				<section id="code">
					<table class="table">
						<thead>
							<tr>
								<th>Product</th>
								<th>Currency</th>
								<th>VAT Percentage</th>
								<th>VAT</th>
								<th>Price excl VAT</th>
								<th>Price incl VAT</th>
								<th>Quantity</th>
								<th>Total VAT</th>
								<th>Total excl VAT</th>
								<th>Total incl VAT</th>
							</tr>
						</thead>
						<tbody>
							<?php 
								foreach ( $prices as $price )
								{
									$quantity = 5 + ( $quantity * $quantity ) % 33;
									?>
									<tr>
										<td><?php echo $price->name; ?></td>
										<td><?php echo $price->currency; ?></td>
										<td><?php echo $price->taxPercent; ?></td>
										<td><?php echo $price->formatNumber($price->get_html_price(1, 'getTaxAmount')); ?></td>
										<td><?php echo $price->formatNumber($price->taxA); ?></td>
										<td><?php echo $price->formatNumber($price->get_html_price()); ?></td>
										<td><?php echo $quantity; ?></td>
										<td><?php echo $price->formatNumber($price->get_html_price($quantity, 'getTaxAmount')); ?></td>
										<td><?php echo $price->formatNumber($price->taxA * $quantity); ?></td>
										<td><?php echo $price->formatNumber($price->get_html_price($quantity)); ?></td>
									</tr>
<?php 
								} 
								?>
						</tbody>
					</table>
				</section>
				<script type="text/javascript">
				//<![CDATA[
					$.get( 'README.md', function( data ) {
						var markdown = markdownit();
						$('#information')
						.html( markdown.render( data ) )
						.find( 'h1' )
						.wrap( '<div class="page-header"><\/div>' );
					});
				//]]>
				</script>
			</div>
		</div>
	</div>
</body>
</html>
