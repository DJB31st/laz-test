<?php
// Some class
class product_Price
{
	public $name;
	public $currency;
	public $taxPercent = 25; // tax in percentage
	public $taxA;
	public $price;
	public $fees = ['SKU-11339' => "25"];
	
	function product_Price($price,$currency,$name,$taxPercent="")
	{
		//maybe not clean but means we only need to define default tax % once
		if($taxPercent=="")
			$taxPercent = $this->taxPercent;
		
		$this->name = $name;
		$this->currency = $currency;
		$this->price = $price;
		$this->addFees($this->name);
		$this->taxPercent = $taxPercent;
		$this->taxA = $this->calcTax($this->price,$taxPercent);
		
	}

	/**
	* [setPrice description]
	* @param float $price [description]
	*/
	function setPrice($price, $taxPercent="")
	{
		//maybe not clean but means we only need to define default tax % once
		if($taxPercent=="")
			$taxPercent = $this->taxPercent;
		
		$this->price = $price;
		$this->addFees($this->name);
		$this->taxPercent = $taxPercent;
		$this->taxA = $this->calcTax($this->price,$taxPercent);	
	}
	
	function calcTax($price,$taxPercent)
	{
		return $price / ($taxPercent + 100) * 100;
	}

	function GetPrice($qty=1)
	{
		return ($this->price*$qty);
	}

	function getTaxAmount($qty)
	{
		return ($this->price - $this->taxA) * $qty;
	}
	
	function addFees($name)
	{
		if(array_key_exists($name,$this->fees))
			$this->price += $this->fees[$name];
	}
	
	function get_html_price($qty = 1, $method = 'GetPrice')
	{
		return call_user_method_array($method,$this,[$qty]);
	}
	
	function formatNumber($number)
	{
		switch ($this->currency) 
		{
			case "USD":
				$symbol_left = "&#36;";
				$symbol_right = "";
				break;
			case "EUR":
				$symbol_left = "";
				$symbol_right = "&euro;";
				break;
		}
			
		return $symbol_left.number_format($number,2,".",",").$symbol_right;
	}
}